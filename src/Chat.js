import React from 'react';
import ChatHeader from './components/headerChat/headerChat';
import ChatMessage from './components/messagesChat/message';
import FormChat from "./components/formChat/formChat";

import Header from "./components/header/header";
import Footer from "./components/footer/footer";
import s from './Chat.module.css';

let userSet = new Set();

class Chat extends React.Component {
    state = {
        dataFromServer: [],
        numberMsg: 0,
        numberPersons: 0,
        users: [],
        message: ' ',
        isSpinner: true

    };

    componentDidMount() {
        setTimeout(() => this.setState({isSpinner: false}), 3000);
        this.getDataMessages()

    }

    addMessage = (text, event) => {
        event.preventDefault()
        if (text) {
            this.putNewUserSet('user')
            let curDate = new Date();
            let hours = curDate.getHours();
            let minutes = curDate.getMinutes();
            if (hours < 10) {
                hours = `0${hours}`
            }
            if (minutes < 10) {
                minutes = `0${minutes}`
            }
            const dataFromServer = this.state.dataFromServer;
            dataFromServer.push({
                id: "0",
                text: text,
                user: "You",
                time: `${hours}:${minutes}`,
                classEl: 'userMsg',
            });
            this.setState({numberMsg: this.state.numberMsg + 1});
        }
    }

    deleteMessage = (numMsg) => {
        const dataFromServer = this.state.dataFromServer;
        dataFromServer[numMsg-1].text = "deleted";
        this.setState({numberMsg: this.state.numberMsg - 1});
    }



    putNewUserSet = (user) => {
        userSet.add(user)
    }

    updateData = (value) => {
        this.setState({dataFromServer: value})
    }

    getDataMessages = () => {
        let arr = []
        fetch(`https://edikdolynskyi.github.io/react_sources/messages.json`)
            .then((res) => res.json())
            .then((data) => {
                arr.push(...data)
                data.forEach(el => {
                    this.putNewUserSet(el.user)
                })
                arr.map((el) => {
                    el.time = el.createdAt.substring(11, 16);
                    el.class = 'dislike'
                    el.classEl = 'otherUserMsg'

                });

                this.setState({
                    dataFromServer: arr,
                    numberMsg: arr.length,
                    users: userSet
                })
            });
    };


    render() {

        return (
            <>
                <div className={this.state.isSpinner ? s.loaderWrap : s.none}>
                    <div className={s.loader}/>
                </div>
                <div className={this.state.isSpinner ? s.none : s.container}>
                    <Header/>
                    <ChatHeader dataState={this.state}/>
                    <ChatMessage dataState={this.state} updateData={this.updateData}
                                 deleteMessage={this.deleteMessage}/>
                    <FormChat addMessage={this.addMessage} editMessage={this.editMessage} dataState={this.state} />
                    <Footer/>
                </div>
            </>
        );
    }
}

export default Chat;
