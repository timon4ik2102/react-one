import React from 'react';
import s from './header.module.css';
import Logo from '../assets/img/livechat-vector-logo.svg'

class Header extends React.Component {


    render() {
        return (
            <div className={s.headerWrap}>
                <img className={s.logo} src={Logo} alt="logo"/>

            </div>
        )


    }
}

export default Header;