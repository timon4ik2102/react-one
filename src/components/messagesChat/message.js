import React from 'react';
import s from './message.module.css';

class Message extends React.Component {



    handleLike = (filId) => {
        const {dataFromServer} = this.props.dataState


        let newData = dataFromServer.map(el => {
            let elementClass = el.class;
            if (filId.id === el.id) {

                if (el.class === 'dislike') {
                    elementClass = 'like';
                } else {
                    elementClass = 'dislike';
                }

            }
            return {...el, class: elementClass};
        })

        this.props.updateData(newData)

    };

    handleDelete = () => {
             this.props.deleteMessage(this.props.dataState.numberMsg);
    }




    render() {
        const {dataFromServer} = this.props.dataState;

        console.log(dataFromServer)


        const messageList = dataFromServer.map((field) => (
            <div key={field.id} className={s[field.classEl]}>
                <div className={field.id !== '0' ? s.otherUserImgBlock : s.none}>
                    <img className={s.otherUserImg} src={field.avatar} alt="avatar"/>
                </div>
                <div className={s.otherUserTxtContent}>
                    <div className={s.otherUserTxtAttr}>
                        <div className={s.otherUserDate}>{field.time}</div>
                        <div id={field.id}
                             className={field.id !== '0' ? `${s.otherUserLike} ${s[field.class]}` : s.none}
                             onClick={() => this.handleLike(field)}/>
                        <div id={field.id} className={ field.id === '0'   ? `${s.otherUserLike}` : s.none}>

                            <div id={field.id}
                                 className={field.id === '0' && field.text !== 'deleted' ? s.delBtn : s.none}
                                 onClick={() => this.handleDelete()}>delete
                            </div>
                        </div>
                    </div>
                    <div className={s.otherUserTxt}>{field.text}
                    </div>
                </div>
            </div>
        ));


        return (
            <div className={s.messageWrap}>
                <>{messageList}</>


            </div>
        )


    }
}

export default Message;